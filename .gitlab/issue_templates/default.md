Por favor, descreva o problema da forma mais detalhada possível. Informações úteis que ajudam na solução:

- Como reproduzir: se tiver uma sequência de eventos que causou o problema fica mais fácil encontrar a causa e corrigir
- Comportamento atual: o que está acontecendo de fato no momento
- Comportamento esperado: o que você esperava ver
- Arquivos adicionais: se for um erro que impacta o servidor, como desconexões e erros de sincronização, pode ajudar se colar o [RPT](https://community.bistudio.com/wiki/arma.RPT#Location) do Arma. Se o fizer, por favor use blocos de código com ``` para facilitar a leitura)
- Possível conserto: se souber, anote onde no código está erro, qual módulo, função, etc.
