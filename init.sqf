/*******************************************************************************
Esse script tem inicializações gerais da missão. Recomendo a leitura da ordem
de inicialização para entender o que vem quando:
https://community.bistudio.com/wiki/Initialization_Order

No nosso caso, tudo que esse script faz no momento é retirar os rádios das
unidades para garantir que os rádios certos serão colocados com o script
configurarRadios.sqf
*******************************************************************************/

{ if (isPlayer _x) then
  {
    _x unassignItem "itemRadio";
    _x removeItem "itemRadio";
  };
} foreach (allUnits);
