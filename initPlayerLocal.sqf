/*******************************************************************************
Esse arquivo inicializa comandos e funções específicos aos jogadores. Nesse
caso, apenas carrega o briefing e cria um texto no canto inferior direito da
tela com o nome da missão, mas pode ser modificado para fazer mais coisas.

Verifica se é um servidor e espera para executar até que o jogador tenha ocu-
pado sua posição
*******************************************************************************/

// Verifica se é um servidor (em oposição a cliente) e se o jogador ainda não
// entrou, caso em que espera entrar.
if ((!isServer) && (player != player)) then
{
  waitUntil {player == player};
};

// Carrega o script de briefing.
_null = [] execVM "briefing.sqf";

// Compila o módulo com os teleportes e deixa pronto para executar na missão.
[] call compile preprocessFileLineNumbers "modulos\teleporteTreino.sqf";

["InitializePlayer", [player]] call BIS_fnc_dynamicGroups;

player setVariable ["hasHeli", 0];
player setVariable ["hasAviao", 0];


// Pega o equipamento inicial do jogador e salva para o respawn
player setVariable ["TAG_LoadoutStart", getUnitLoadout player];
