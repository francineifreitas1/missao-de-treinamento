/*******************************************************************************
  Autor: Augusto "Lango" Moura

	Descrição:
		Executa o treinamento de pilotagem de helicópteros.

	Parameter(s):
		0: objeto - unidade que vai participar do treinamento

	Returns:
		BOOLEAN

	Examples:
		[player] call caju_fnc_heli;

*******************************************************************************/
// Parâmetros da função chamada na placa para começar o treinamento
params [
  ["_unit", objNull, [objNull]]
  ];

_unit linkitem "ItemMap";
_unit linkitem "ItemCompass";

// Se por acaso o valor ficar menor que 0, ele limita em zero
if (heliTrainee < 0) then {
    heliTrainee = 0;
};

heliTrainee = heliTrainee + 1;
publicVariable "heliTrainee";

if (heliTrainee > 8) exitWith {
    systemChat "Excedido número de helicópteros, aguarde alguém liberar.";
};

_hasHeli = _unit getVariable "hasHeli";

if (_hasHeli >= 1) exitWith {
    systemChat "Você já tem um helicóptero, deixa de ser fominha."
};

_hasHeli = _hasHeli + 1;

_unit setVariable [ "hasHeli", _hasHeli];

_heliPad = "heliPad_" + str heliTrainee;
_heliPad = missionNamespace getVariable _heliPad;

// Seleção de helicóptero
systemChat "Escolha o helicóptero que deseja pilotar com o menu de ação.";

heliType = "";

_unit addAction ["AH1Z 'Viper'", {heliType = "UK3CB_MDF_O_AH1Z_NAVY"}];
_unit addAction ["AH-6M 'Little Bird' (armado)", {heliType = "RHS_MELB_AH6M"}];
_unit addAction ["AH-64D 'Apache'", {heliType = "RHS_AH64D_wd"}];
_unit addAction ["AW-101 'Merlin'", {heliType = "I_Heli_Transport_02_F"}];
_unit addAction ["AW-159 'Wildcat'", {heliType = "I_Heli_light_03_unarmed_F"}];
_unit addAction ["AW-159 'Wildcat' (armado)", {heliType = "I_Heli_light_03_dynamicLoadout_F"}];
_unit addAction ["CH-47F 'Chinook'",{heliType = "RHS_CH_47F_light";}];
_unit addAction ["CH-49 'Mohawk'", {heliType = "B_UN_Heli_Transport_02_lxWS"}];
_unit addAction ["CH-53E 'Super Stallion'", {heliType = "rhsusf_CH53E_USMC_D"}];
_unit addAction ["CH-53E 'Super Stallion' (arma traseira)", {heliType = "rhsusf_CH53E_USMC_GAU21"}];
_unit addAction ["Ka-60 'Kasatka'",{heliType = "rhs_ka60_c";}];
_unit addAction ["Ka-60 'Kasatka' (armado)", {heliType = "O_Heli_Light_02_dynamicLoadout_F"}];
_unit addAction ["Mi-8MT 'Hip'",{heliType = "RHS_Mi8mt_vvsc";}];
_unit addAction ["Mi-8MT 'Hip' (armado)", {heliType = "UK3CB_TKA_O_Mi8AMTSh"}];
_unit addAction ["Mi-24 'Hind'", {heliType = "UK3CB_CW_SOV_O_LATE_Mi_24V"}];
_unit addAction ["Mi-48 'Kajman'", {heliType = "O_SFIA_Heli_Attack_02_dynamicLoadout_lxWS"}];
_unit addAction ["Mi-290 'Taru'", {heliType = "O_Heli_Transport_04_bench_F"}];
_unit addAction ["OH-6M 'Littlebird'",{heliType = "RHS_MELB_H6M";}];
_unit addAction ["PO-30 'Orca'", {heliType = "B_ION_Heli_Light_02_unarmed_lxWS"}];
_unit addAction ["PO-30 'Orca' (armado)", {heliType = "B_ION_Heli_Light_02_dynamicLoadout_lxWS"}];
_unit addAction ["RAH-66 'Comanche'", {heliType = "B_D_Heli_Attack_01_dynamicLoadout_lxWS"}];
_unit addAction ["UH-1H 'Huey'",{heliType = "UK3CB_ADA_O_UH1H";}];
_unit addAction ["UH-1H 'Huey' (armas laterais)", {heliType = "UK3CB_O_G_UH1H_M240_FIA"}];
_unit addAction ["UH-1H 'Huey' (armado)", {heliType = "UK3CB_O_G_UH1H_GUNSHIP_FIA"}];
_unit addAction ["UH-60M 'Blackhawk'",{heliType = "RHS_UH60M2_d";}];
_unit addAction ["UH-60 'Black Hawk' (armado)", {heliType = "RHS_UH60M_ESSS"}];
_unit addAction ["UH-80 'Ghost Hawk'", {heliType = "B_D_Heli_Transport_01_lxWS"}];

waitUntil {sleep 2; heliType != ""};

_veh = heliType createVehicle (getPos _heliPad);

// Radio para o Huey, 'Nam vibes
if (heliType == "UK3CB_ADA_O_UH1H" or heliType == "UK3CB_O_G_UH1H_M240_FIA" or heliType == "UK3CB_O_G_UH1H_GUNSHIP_FIA") then {
  _sndsrc = _veh say3D ["fortunate", 100, 1, false, 5];
  _veh addAction ["Desligar Radio", // Possibilidade de desligar a música
  {
    deleteVehicle (_this select 3);
  },
  _sndsrc
  ];
};

_veh setDir 315;

removeAllActions _unit;

_unit moveInDriver _veh;

_unitGroup = units group _unit - [_unit];

if (count _unitGroup == 1) then {
    (_unitGroup select 0) moveInGunner _veh;
} else {
  {
      _x moveInAny _veh;
  } forEach _unitGroup;
};

systemChat "As ações relacionadas ao treinamento começam com 'T'.";

_unit addAction ["T - Curar/consertar", {
  [(_this select 1)] call ace_medical_treatment_fnc_fullHealLocal;
  vehicle (_this select 1) setDamage 0
}];

_veh addAction ["T - Perda do rotor traseiro", {
  (_this select 0) setHitPointDamage ['hitvrotor', 1.0];
}];

_veh addAction ["T - Autorrotação", {
  (_this select 0) setHitPointDamage ['hitengine', 1.0];
}];

_veh setVariable ["ace_cookoff_enable", false, true];

// EventHandler para remover o slot do jogador no treino caso ele morra.
if (isNil {_unit getVariable "RespawnEH"}) then {
  _unit setVariable [
    "RespawnEH", _unit addEventHandler ["Respawn",
      {
        deleteVehicle (objectParent (_this select 1));
        heliTrainee = heliTrainee - 1;
        publicVariable "heliTrainee";
        (_this select 0) removeEventHandler ["Respawn", _thisEventHandler];
        (_this select 0) setVariable ["hasHeli", 0];
        (_this select 0) setVariable ["RespawnEH", nil];
        (_this select 0) setPos (markerPos "marcHeli");
      }
    ]
  ];
};

_unit addAction ["T - Encerrar",
  {
    heliTrainee = heliTrainee - 1;
    publicVariable "heliTrainee";
    (_this select 1) call caju_fnc_limpaEquip;
    [(_this select 1)] call ace_medical_treatment_fnc_fullHealLocal;
    (_this select 1) setPos (markerPos "respawn");
    (_this select 1) setVariable ["hasHeli", 0];
    deleteVehicle (_this select 3);
    _ehIndex = (_this select 1) getVariable "RespawnEH";
    if (!isNil "_ehIndex") then {
      (_this select 1) removeEventHandler ["Respawn", _ehIndex];
      (_this select 1) setVariable ["RespawnEH", nil];
    };
    removeAllActions (_this select 1)
  },
  _veh
];
