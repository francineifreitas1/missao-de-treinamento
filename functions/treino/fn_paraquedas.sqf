/*******************************************************************************
  Autor: Augusto "Lango" Moura

	Descrição:
		Executa o treinamento de salto de paraquedas.

    Parameter(s):
  		0: objeto - unidade que vai participar do treinamento
      1: objeto - a placa da qual o treinamento foi executado
      2: string - argumentos para identificar o tipo de treinamento para a limpeza
                  subsequente

  	Returns:
  		BOOLEAN

  	Examples:
  		[player, placaPI, "treinoPQ"] call caju_fnc_paraquedas;

*******************************************************************************/
// Parâmetros da função chamada na placa para começar o treinamento
params [
  ["_unit", objNull, [objNull]],
  ["_placa", objNull, [objNull]],
  ["_arguments", "treinoPQ", [""]]
  ];

// Alvos para o paraquedas
_chuteTargetArray = [
  "marcChute_0",
  "marcChute_1",
  "marcChute_2",
  "marcChute_3",
  "marcChute_4",
  "marcChute_5",
  "marcChute_6",
  "marcChute_7",
  "marcChute_8",
  "marcChute_9"
];
_chuteTarget = selectRandom _chuteTargetArray;
_chuteTarget setMarkerType "mil_end";

// Cria o avião numa posição predefinida
_aviaoArray = [(markerPos "marcAviao" vectorAdd [0,0,3000]), 270, "RHS_C130J", east] call BIS_fnc_spawnVehicle;
_aviao = _aviaoArray select 0;
_aviaoTrip = _aviaoArray select 2;

// Direciona o avião para o alvo e cria o waypoint
_direction = _aviao getDir (markerPos _chuteTarget);
_direction = _aviao getDir (markerPos _chuteTarget);
_aviao setDir _direction;

_aviao engineOn true;
_aviao flyInHeight 3000;
_wp1 = _aviaoTrip addWaypoint [markerpos _chuteTarget, 200];
_wp1 setWaypointSpeed "FULL";

// Define se o jogador vai sozinho ou se todos no grupo vão
if (_placa == placaPE) then {
    _seat = 1;
    {
      _x moveInCargo [_aviao, _seat];
      _x addBackpack "B_Parachute";
      _seat = _seat + 1;
    } forEach units group _unit;
} else {
    // Coloca o jogador no avião e adiciona paraquedas
    _unit moveInCargo [_aviao, 1];
    _unit addBackpack "B_Parachute";
};

// Cria as luzes para o salto
_whitelight =  createVehicle ["Land_Camping_Light_F", [0,0,0], [], 0, "NONE"];
_redlight = createVehicle ["Land_TentLamp_01_suspended_red_F", [0,0,0], [], 0, "NONE"];
_greenlight = createVehicle ["Reflector_Cone_01_wide_green_F", [0,0,0], [], 0, "NONE"];
_whitelight attachTo [_aviao, [0, 10, 4]];

_jumpCheck = markerPos _chuteTarget vectorAdd [0,0,3000];

waitUntil {
  (_unit distance _jumpCheck) < 2000
};

_whitelight attachTo [_aviao, [0, 8, 100] ];
_redlight attachTo [_aviao, [0, 3, 0] ];
_aviao animate ["ramp_bottom", 1];
_aviao animate ["ramp_top", 1];
sleep 10;
_whitelight attachTo [_aviao, [0, 8, 100] ];
_redlight attachTo [_aviao, [0, -6, 100] ];
_greenlight attachTo [_aviao, [0, -15, 4] ];

// Ejeta os passageiros
{
  moveOut _x;
  unassignVehicle _x;
  sleep 0.3;
} forEach assignedCargo _aviao;

deleteVehicle _whitelight;
deleteVehicle _redlight;
deleteVehicle _greenlight;

// Cria fumaça no alvo
_posATL = markerPos _chuteTarget;
sinalFumaca = "#particlesource" createVehicleLocal _posATL;
sinalFumaca setParticleParams [
	["\A3\Data_F\ParticleEffects\Universal\Universal", 16, 7, 16, 1], "", "Billboard",
	1, 8, [0, 0, 0], [0, 0, 2.5], 0, 10, 7.9, 0.066, [4, 12, 20],
	[[1, 0, 0, 0], [1, 0.05, 0.05, 1], [1, 0.05, 0.05, 1], [1, 0.05, 0.05, 1], [1, 0.1, 0.1, 0.5], [1, 0.125, 0.125, 0]],
	[0.25], 1, 0, "", "", sinalFumaca];
sinalFumaca setParticleRandom [0, [0.25, 0.25, 0], [0.2, 0.2, 0], 0, 0.25, [0, 0, 0, 0.1], 0, 0];
sinalFumaca setDropInterval 0.2;

// Placa no objetivo
_posPlaca = markerPos _chuteTarget vectorAdd [15,15,0];
placaObj = "Sign_F" createVehicle _posPlaca;
textObj = "UserTexture1m_F" createVehicle _posPlaca;
textObj setObjectTexture [0, "figs\retorno.jpg"];
textObj attachTo [placaObj, [0,-0.05,0.6]];
placaObj addaction ["Retornar para início","modulos\retornaInicio.sqf", _argument];

{
    waitUntil {isTouchingGround _x};
    _dist = _x distance (markerPos _chuteTarget);
    _resultado = "Distância do alvo: "+ str (round _dist) + " m";
    _resultado remoteExec ["hint", _x];
} forEach units group _unit;

// Apaga o avião de salto
{
  _aviao deleteVehicleCrew _x
} forEach crew _aviao;
deleteVehicle _aviao;

// Apaga o marcador no mapa
deleteMarker _chuteTarget;
