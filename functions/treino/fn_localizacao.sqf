/*******************************************************************************
  Autor: Augusto "Lango" Moura

	Descrição:
		Executa o treinamento de localização.

	Parameter(s):
		0: objeto - unidade que vai participar do treinamento
    1: objeto - a placa da qual o treinamento foi executado
    2: string - argumentos para identificar o tipo de treinamento para a limpeza
                subsequente

	Returns:
		BOOLEAN

	Examples:
		[player, placaPI, "treinoLoc"] call caju_fnc_localizacao;

*******************************************************************************/
// Parâmetros da função chamada na placa para começar o treinamento
params [
  ["_unit", objNull, [objNull]],
  ["_placa", objNull, [objNull]],
  ["_argument", "treinoLoc", [""]]
  ];

// Adiciona binóculos e bússola para o jogador
_unit addWeapon "Binocular";
_unit linkitem "ItemCompass";
_unit linkitem "ItemMap";

// Os dois objetivos possíveis para terminar o treino
_listaObj = ["marc_locFim_0", "marc_locFim_1"];
_marcObj = selectRandom _listaObj;

_listaInicio = [];
_obj = "";

// Seleciona a posição inicial em função do objetivo escolhido.
switch (_marcObj) do {
    case ("marc_locFim_0"): {
      _listaInicio = [
      "marc_locInicio_0",
      "marc_locInicio_1",
      "marc_locInicio_2",
      "marc_locInicio_3",
      "marc_locInicio_4",
      "marc_locInicio_5",
      "marc_locInicio_6",
      "marc_locInicio_7",
      "marc_locInicio_8",
      "marc_locInicio_9"];
      _obj = "Jangali";
    };
    case ("marc_locFim_1"): {
      _listaInicio = [
      "marc_locInicio_10",
      "marc_locInicio_11",
      "marc_locInicio_12",
      "marc_locInicio_13",
      "marc_locInicio_14",
      "marc_locInicio_15",
      "marc_locInicio_16",
      "marc_locInicio_17",
      "marc_locInicio_18",
      "marc_locInicio_19"];
      _obj = "Chinari";
    };
};

_marcInicio = selectRandom _listaInicio;

// Cria as placas para o jogador retornar para o início
// Placa no objetivo
placaObj = "Sign_F" createVehicle (markerPos _marcObj);
textObj = "UserTexture1m_F" createVehicle (markerPos _marcObj);
textObj setObjectTexture [0, "figs\retorno.jpg"];
textObj attachTo [placaObj, [0,-0.05,0.6]];
placaObj addaction ["Retornar para início","modulos\retornaInicio.sqf",_argument];

// Placa no início
placaInicio = "Sign_F" createVehicle markerPos _marcInicio;
textInicio = "UserTexture1m_F" createVehicle markerPos _marcInicio;
textInicio setObjectTexture [0, "figs\retorno.jpg"];
textInicio attachTo [placaInicio, [0,-0.05,0.6]];
placaInicio addaction ["Retornar para início","modulos\retornaInicio.sqf",_argument];

// Teleporta o jogador para o início;
_unit setPos (markerPos _marcInicio) findEmptyPosition [0,5];

sleep 0.5;

// Define o objetivo como tarefa atual
[_unit, "tskLoc", ["Encontre o destino.", "Chegue em "+_obj, _marcObj], markerPos _marcObj, "ASSIGNED"] call BIS_fnc_taskCreate;

// Abaixo são adicionadas instruções no briefing do mapa para ajudar na localização
_unit createDiarySubject ["Loc", "Treino Localização"]; // cria uma nova categoria de briefing

// Cria os itens no briefing
_instLoc0 = _unit createDiaryRecord ["Loc", ["Dicas", "Se estiver numa cidade, o jeito mais fácil é ir até o limite dela, onde sempre tem uma placa com o nome:<br/><img image='figs\loc\cidade.jpg' width='268' height='200'/><br/><br/>A partir daí, basta descobrir em qual lado da cidade está. <br/> Sua bússola é sua maior aliada. Como ela aparece no mapa na mesma direção que você está olhando, é possível triangular sua posição utilizando pontos de referência. Observe o exemplo abaixo com a torre:<br/><img image='figs\loc\torre.jpg' width='200' height='487'/><br/><br/>O mapa também destaca itens importantes. Prédios são sempre visíveis como blocos cinza:<br/><img image='figs\loc\predios.jpg' width='340' height='200'/><br/><br/>E estradas são marcadas pela sua importância:<br/><img image='figs\loc\estradas.jpg' width='273' height='200'/>"]];
_instLoc1 = _unit createDiaryRecord ["Loc", ["Instruções", "Você está começando em algum lugar aleatório em um raio de 1,5 km do objetivo. Para chegar no objetivo, precisa descobrir onde está. Munido de uma bússola, essa tarefa não é tão difícil.<br/><br/>Primeira coisa é tentar encontrar pontos importantes de referência, como cidades, prédios específicos como torres de rádio, estradas, a costa, etc. Alcançar um lugar alto permite também visualizar melhor os arredores.<br/><br/>Você está começando num lugar aleatório mais ou menos difícil, mas em qualquer um deles é possível se localizar. Observe primeiro as características do terreno: há colinas, vales, bosques próximos? Dificilmente vão permitir se localizar por si só, mas vão ajudar a refinar sua posição.<br/><br/>Se quiser retornar, só utilizar a placa para retorno próxima de você."]];
_instLoc2 = _unit createDiaryRecord ["Loc", ["Objetivo", "Chegar até o ponto marcado no mapa (vide 'Tasks').<br/> Chegando lá, haverá uma placa de retorno para o aeroporto."]];
