/*******************************************************************************
  Autor: Augusto "Lango" Moura

	Descrição:
		Executa o treinamento de pilotagem de aviões.

	Parameter(s):
		0: objeto - unidade que vai participar do treinamento

	Returns:
		BOOLEAN

	Examples:
		[player] call caju_fnc_aviao;

*******************************************************************************/
// Parâmetros da função chamada na placa para começar o treinamento
params [
  ["_unit", objNull, [objNull]]
  ];

_unit linkitem "ItemMap";
_unit linkitem "ItemCompass";

// Se por acaso o valor ficar menor que 0, ele limita em zero
if (aviaoTrainee < 0) then {
    aviaoTrainee = 0;
};

aviaoTrainee = aviaoTrainee + 1;
publicVariable "aviaoTrainee";

if (aviaoTrainee > 6) exitWith {
    systemChat "Excedido número de aviões, aguarde alguém liberar.";
};

_hasAviao = _unit getVariable "hasAviao";

if (_hasAviao >= 1) exitWith {
    systemChat "Você já tem um avião, deixa de ser fominha."
};

_hasAviao = _hasAviao + 1;

_unit setVariable [ "hasAviao", _hasAviao];

_aviaoPad = "aviao_" + str aviaoTrainee;
_aviaoPad = missionNamespace getVariable _aviaoPad;

// Seleção de helicóptero
systemChat "Escolha o avião que deseja pilotar com o menu de ação.";

aviaoType = "";

_unit addAction ["Aero Commander 500",{aviaoType = "UK3CB_C_AC500";}];
_unit addAction ["Aero L-39 'Albatros'", {aviaoType = "UK3CB_AAF_B_L39_AA";}];
_unit addAction ["Antonov An-2",{aviaoType = "UK3CB_C_Antonov_An2_CH";}];
_unit addAction ["A-10A 'Thunderbolt II'", {aviaoType = "UK3CB_CW_US_B_EARLY_A10";}];
_unit addAction ["A-10D 'Thunderbolt II'", {aviaoType = "B_Plane_CAS_01_dynamicLoadout_F";}];
_unit addAction ["A-29 'Super Tucano'", {aviaoType = "RHSGREF_A29B_HIDF";}];
_unit addAction ["Cessna 172 Skyhawk",{aviaoType = "UK3CB_C_Cessna_172";}];
_unit addAction ["Cessna TTx",{aviaoType = "C_Plane_Civil_01_F";}];
_unit addAction ["C130",{aviaoType = "RHS_C130J";}];
_unit addAction ["F/A-181 'Black Wasp II'", {aviaoType = "B_Plane_Fighter_01_F";}];
_unit addAction ["F-22A 'Raptor'", {aviaoType = "rhsusf_f22";}];
_unit addAction ["JAS 39 'Grippen'", {aviaoType = "UK3CB_AAF_B_Gripen";}];
_unit addAction ["L-159 'ALCA'", {aviaoType = "rhs_l159_cdf_b_CDF";}];
_unit addAction ["MD-454 'Mystere'", {aviaoType = "UK3CB_MDF_B_Mystere";}];
_unit addAction ["MiG-29 'Fulcrum'", {aviaoType = "UK3CB_ADA_B_MIG29S";}];
_unit addAction ["Su-25 'Frogfoot'", {aviaoType = "RHS_Su25SM_vvsc";}];
_unit addAction ["Su-57 'Felon'", {aviaoType = "RHS_T50_vvs_blueonblue";}];
_unit addAction ["To-201 'Shilkra'", {aviaoType = "O_Plane_Fighter_02_F";}];
_unit addAction ["Tu-95 'Bear'", {aviaoType = "RHS_TU95MS_vvs_old";}];
_unit addAction ["T-41 'Mescalero'", {aviaoType = "UK3CB_AAF_B_Cessna_T41_Armed";}];
_unit addAction ["V-44 X 'Blackfish'", {heliType = "B_T_VTOL_01_infantry_F"}];
_unit addAction ["V-44 X 'Blackfish' (armado)", {heliType = "B_T_VTOL_01_armed_F"}];
_unit addAction ["Yak-130", {aviaoType = "O_Plane_CAS_02_dynamicLoadout_F";}];
_unit addAction ["Y-32 'Xi an'", {heliType = "O_T_VTOL_02_infantry_dynamicLoadout_F"}];

waitUntil {sleep 2; aviaoType != ""};

_dir = 0;

if (aviaoType == "RHS_C130J" or aviaoType == "RHS_TU95MS_vvs_old") then {
    _aviaoPad = c130Pad;
    _dir = 225;
} else {
    _dir = 315;
};

_veh = aviaoType createVehicle (getPos _aviaoPad);

_veh setDir _dir;

removeAllActions _unit;

_unit moveInDriver _veh;

_unitGroup = units group _unit - [_unit];

if (count _unitGroup == 1) then {
    (_unitGroup select 0) moveInGunner _veh;
} else {
  {
      _x moveInAny _veh;
  } forEach _unitGroup;
};

systemChat "As ações relacionadas ao treinamento começam com 'T'.";

_veh setVariable ["ace_cookoff_enable", false, true];

_unit addAction ["T - Curar/consertar", {
  [(_this select 1)] call ace_medical_treatment_fnc_fullHealLocal;
  vehicle (_this select 1) setDamage 0
}];

_veh setVariable ["ace_cookoff_enable", false, true];

// EventHandler para remover o slot do jogador no treino caso ele morra.
if (isNil {_unit getVariable "RespawnEH"}) then {
  _unit setVariable [
    "RespawnEH", _unit addEventHandler ["Respawn",
      {
        deleteVehicle (objectParent (_this select 1));
        aviaoTrainee = aviaoTrainee - 1;
        publicVariable "aviaoTrainee";
        (_this select 0) removeEventHandler ["Respawn", _thisEventHandler];
        (_this select 0) setVariable ["hasAviao", 0];
        (_this select 0) setVariable ["RespawnEH", nil];
        (_this select 0) setPos (markerPos "pilotaAviao");
      }
    ]
  ];
};

_unit addAction ["T - Encerrar",
  {
    aviaoTrainee = aviaoTrainee - 1;
    publicVariable "aviaoTrainee";
    (_this select 1) call caju_fnc_limpaEquip;
    [(_this select 1)] call ace_medical_treatment_fnc_fullHealLocal;
    (_this select 1) setPos (markerPos "respawn");
    (_this select 1) setVariable ["hasAviao", 0];
    deleteVehicle (_this select 3);
    _ehIndex = (_this select 1) getVariable "RespawnEH";
    if (!isNil "_ehIndex") then {
      (_this select 1) removeEventHandler ["Respawn", _ehIndex];
      (_this select 1) setVariable ["RespawnEH", nil];
    };
    removeAllActions (_this select 1)
  },
  _veh
];
