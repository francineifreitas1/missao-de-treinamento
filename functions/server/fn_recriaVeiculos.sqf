/*******************************************************************************
  Autor: Augusto "Lango" Moura

	Descrição:
		Recria veículos destruídos automaticamente no mesmo lugar.

	Parameter(s):
		0: objeto - veículo a ser recriado

	Returns:
		BOOLEAN

	Examples:
		No init do veículo:
    this spawn caju_fnc_recriaVeiculos;

*******************************************************************************/
params ["_veh"]; //Pega o veículo em que foi executada a função

// Executa somente no servidor, caso contrário recria um veículo para cada
// jogador mais um para o servidor
if (!isServer) exitWith {};

// Variável para monitorar o estado do veículo
private _monitor = true;

// Pega o tipo e posição do veículo;
_vehType = getDescription _veh select 0;
_pos = getPos _veh;

// Monitora o estado do veículo e executa o código quando destruído
while {_monitor} do {
    sleep 10;
    if (!alive _veh) then {
      sleep 10;
      deleteVehicle _veh;
      _monitor = false;
      sleep 0.1;
      _veh = _vehType createVehicle _pos;
      _veh setFuel 0;
      _veh setVehicleAmmo 0;
    };
};

// Executa novamente a função no novo veículo criado para recriar infinitamente
_veh spawn caju_fnc_recriaVeiculos;
