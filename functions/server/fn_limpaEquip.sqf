/*******************************************************************************
  Autor: Augusto "Lango" Moura

	Descrição:
		Reseta o equipamento do jogador para o equipamento inicial.

	Parameter(s):
		0: objeto - unidade para resetar o equipamento
	Returns:
		BOOLEAN

	Examples:
		this spawn caju_fnc_limpaEquip;

*******************************************************************************/
params ["_unit"]; //Pega o veículo em que foi executada a função

removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeVest _unit;
removeBackpack _unit;
removeGoggles _unit;
removeHeadgear _unit;

// Força a usar o uniforme e capacetes originais da missão
_unit forceAddUniform "rhs_uniform_afghanka";
_unit addHeadgear "rhs_ssh68_2";
_unit linkitem "ItemMap";
