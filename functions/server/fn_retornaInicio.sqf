/*******************************************************************************
  Autor: Augusto "Lango" Moura

	Descrição:
    Retorna o usuário para o início da missão de treinamento, removendo qualquer
  equipamento que ele tenha equipado. Vale para todas as placas de retorno.

	Parameter(s):
		0: objeto - unidade para resetar o equipamento
    1: objeto - a placa de onde foi acionada a função
    2: string - argumentos para executar opções específicas
	Returns:
		BOOLEAN

	Examples:
		[player, placaPE, "treinoLoc"] call caju_fnc_retornaInicio;

*******************************************************************************/
params [
  ["_unit", objNull, [objNull]],
  ["_placa", objNull, [objNull]],
  ["_argument", "none", [""]]
  ];

{
  _x call caju_fnc_limpaEquip;
  _x setPos (markerPos "respawn");
  _x call ace_medical_treatment_fnc_fullHealLocal;
} forEach units group _unit;

/******************************************************************************/
/* Essa parte é para "limpar" o que foi adicionado/modificado no treinamento  */
/* de localização e paraquedismo                                              */
/******************************************************************************/

// Remove as entradas no briefing
_exists = _unit diarySubjectExists "Loc";
if (_exists) then {
    _unit removeDiarySubject "Loc";
};
_exists = _unit diarySubjectExists "BD1";
if (_exists) then {
    _unit removeDiarySubject "BD1";
};

// Retorno do treino de localização
if (_argument == "treinoLoc") then {
  // Define o resultado da tarefa gerada.
  _taskExists = "tskLoc" call BIS_fnc_taskExists;
  if (_taskExists) then {
    if (_placa == placaInicio) then {
      ["tskLoc", "FAILED"] call BIS_fnc_taskSetState;
    };
    if (_placa == placaObj) then {
      ["tskLoc", "SUCCEEDED"] call BIS_fnc_taskSetState;
    };
  };
  // Apaga as placas utilizadas
  if (!isNil "placaInicio") then {
      deleteVehicle placaInicio;
      deleteVehicle textInicio;
  };
  if (!isNil "placaObj") then {
      deleteVehicle placaObj;
      deleteVehicle textObj;
  };
};

if (_argument == "treinoPQ") then {
  // Apaga a fumaça do alvo de paraquedismo
  if (!isNil "sinalFumaca") then {
      deleteVehicle sinalFumaca;
  };
};

if (_argument == "treinoBD1") then {
  // Apaga a fumaça do alvo de paraquedismo
  deleteMarker inicio;
  deleteMarker objetivo;
};
