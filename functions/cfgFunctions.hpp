/*******************************************************************************
Arquivo com definição das funções para preprocessamento automático pelo jogo.

Segue o formato requerido e explicado em:

https://community.bistudio.com/wiki/Arma_3:_Functions_Library

Autor: Augusto "Lango" Moura
*******************************************************************************/

class caju {
  class server {
    class recriaVeiculos {};
    class limpaEquip {};
    class retornaInicio {};
  };
  class treino {
    class paraquedas {};
    class localizacao {};
    class heli {};
    class aviao {};
  };
};
