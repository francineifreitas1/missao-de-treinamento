/*******************************************************************************
Esse arquivo cria o briefing da missão, disponível no mapa. O briefing deve
ser colocado em casos diferentes dependendo do lado que se está jogando:

case WEST: BlUFOR (azul)
case EAST: OPFOR (vermelho)
case RESISTANCE: INDEPENDENT (verde)
case CIVILIAN: CIVILIAN (roxo)

IMPORTANTE: cada novo "diary" adicionado entra abaixo do anterior na exibição
do mapa. Portanto, você deve adicioná-los na ordem inversa à que deseja que
apareçam na sua missão.

Alguns comandos úteis:
- Acrescentar link para um marcador:
<marker name='nome_marcador'>texto de exibição</marker>
- Acrescentar imagem:
<img image='imagem.jpeg ou paa' />
- Alterar fonte:
<font color='#FF0000' size='14' face='vbs2_digital'>Texto de exibição</font>
- Nova linha (utilize duas vezes se quiser espaço entre as linhas):
<br/>
*******************************************************************************/

// Isso apenas verifica se os jogadores já ocuparam suas posições
waitUntil { !isNil {player} };
waitUntil { player == player };

// Cria o briefing para a facção apropriada
switch (side player) do
{

  case WEST: // Instruções para BlUFOR
  {
  };

  case EAST: // Instruções para OPFOR
  {
    player createDiaryRecord ["Diary", ["Instruções", "Missão de Treinamento do Comando Carcaju.<br/><br/>Selecione a atividade que quiser nas placas e será levado até ela. Para voltar, use a placa de retorno no local de treino.<br /><br />Para fazer treinamentos em equipe, use o menu de grupos (tecla padrão U) e crie um grupo, convide quem quiser."]];
  };

  case RESISTANCE: // Instruções para INDEPENDENT
  {
  };

  case CIVILIAN: // Instruções para CIVILIAN
  {
  };

};
