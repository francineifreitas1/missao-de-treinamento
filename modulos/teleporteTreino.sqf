/*******************************************************************************
Esse arquivo define as ações (scroll do mouse) adicionadas às três placas
iniciais para iniciar os diferentes treinamentos
*******************************************************************************/

/*******************************************************************************
Placa de treinamentos individuais
*******************************************************************************/
// Treinamento no campo de tiro
placaPI addaction ["Campo de Tiro",{(_this select 1) setpos (markerpos "marcPontaria")}];

// Treinamento de localização
placaPI addaction ["Uso de mapa e localização",{[_this select 1, _this select 0, _this select 3] spawn caju_fnc_localizacao},"treinoLoc"];

// Treinamento de paraquedas
placaPI addaction ["Salto de paraquedas",{[_this select 1, _this select 0, _this select 3] spawn caju_fnc_paraquedas},"treinoPQ"];

// Treinamento asa rotativa
placaPI addaction ["Asa rotativa",{(_this select 1) setpos (markerpos "marcHeli")}];

// Treinamento asa fixa
placaPI addaction ["Asa fixa",{(_this select 1) setpos (markerpos "pilotaAviao")}];

/*******************************************************************************
Placa de prática em grupo
*******************************************************************************/
// Treinamento de paraquedas
placaPE addaction ["Salto de paraquedas","modulos\paraquedas.sqf","treinoPQ"];

// Exercício de combate 1
//placaPE addaction ["EC1 - Reação a fogo direto", "modulos\combate1.sqf", "treinoBD1"];

/*******************************************************************************
Placa de retorno do treinamento de tiro
*******************************************************************************/
placaRetorno addaction ["Retornar para início",{[_this select 1, _this select 0, _this select 3] spawn caju_fnc_retornaInicio}];

/*******************************************************************************
Placa asa rotativa
*******************************************************************************/
placaHeli addaction ["Pilotagem",{(_this select 1) spawn caju_fnc_heli}];

placaHeli addaction ["Retornar para início",{[_this select 1, _this select 0, _this select 3] spawn caju_fnc_retornaInicio}];

/*******************************************************************************
Placa asa fixa
*******************************************************************************/
placaAviao addaction ["Pilotagem",{(_this select 1) spawn caju_fnc_aviao}];

placaAviao addaction ["Retornar para início",{[_this select 1, _this select 0, _this select 3] spawn caju_fnc_retornaInicio}];
